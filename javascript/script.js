let chooseFiles = [];

document.querySelector('input[name="fileInput"]').addEventListener("click", (e) => e.target.value = ""); 

document.querySelector('input[name="fileInput"]').addEventListener("change", (e) => {
  chooseFiles = e.target.files;
  document.querySelector(".fileList").innerHTML = "";
  draw();
});

document.querySelector(".upload-btn").addEventListener("click", () => upload());

function draw(){
  for(let i = 0; i < chooseFiles.length; i++){
    let item = document.createElement("item");
    item.setAttribute("class", "item");
    item.setAttribute("data", chooseFiles[i].name);
    document.querySelector(".fileList").append(item);

    let img = document.createElement("img");
    img.setAttribute("class", "img");
    img.setAttribute("src", "image/icon-upload.png");
    item.append(img);

    let contain = document.createElement("div");
    contain.setAttribute("class", "item-contain")
    item.append(contain);

    let title = document.createElement("span");
    title.setAttribute("class", "title");
    title.innerHTML = chooseFiles[i].name;
    contain.append(title);

    let info = document.createElement("info");
    info.setAttribute("class", "item-info");

    let progress = document.createElement("progress");
    progress.setAttribute("class", "progress");
    progress.setAttribute("value", 0);
    progress.setAttribute("max", 100);
    info.append(progress);

    let percent = document.createElement("percent");
    percent.setAttribute("class", "percent");
    percent.innerHTML = "0%";
    info.append(percent);
    contain.append(info);
  }
  if(fileUpload.files.length > 0){
    document.getElementsByClassName("upload-btn")[0].style.display = "block";
    document.getElementsByClassName("fileList")[0].style.display = "block";
  }
}

let y = 3;
let x = 0;
function upload() {
	for(let i = x; i < x + y; i++) {
		if(chooseFiles[i]) {
			let xhr = new XMLHttpRequest();
			let form = new FormData();
			form.append('file', chooseFiles[i]);
			let fileName = chooseFiles[i].name;
			xhr.onreadystatechange = function() {
				if(this.readyState == XMLHttpRequest.DONE) {
					let data = JSON.parse(this.responseText);
					if(data.status) {
						x++;
						if(x % y == 0) {
							upload();
						}
					}
				}
			}
			xhr.upload.onprogress = function(e) {
				let percent = Math.round((e.loaded / e.total) * 100);
				let item = document.querySelector(`.item[data ='${fileName}']`);
				item.querySelector('.percent').innerText = `${percent}%`;
				item.querySelector('.progress').setAttribute('value', percent);
			}
			xhr.open('POST', "http://192.168.0.107/");
			xhr.send(form);
		}
	}
}